# Redis event-store sample
Simple sample of the web service sending IBM stock price. 
Data are published by Redis pubsub and websocket (Sock.js). 
Every event is also stored using Redis Streams for later processing (e.g. Event Sourcing).

## What is Event-Store
It is a place to save event-based communication with in order of time. It could be implemented with publisher, subscriber or message broker.

Every event is stored with timestamp. When you need to stream events in order, you just specify range (las day) and push them into message broker. 

## About Redis Streams
Recently added Redis functionality (from v5). Has ability to define named streams and append records by its timestamp (ms accuary) as a record key. If there would be more records in one ms, Redis solve situation by adding order number to the record key. 

Also has ability to stream saved record in selected range back. 

More about redis streams at https://redis.io/topics/streams-intro

## Install - Docker
1. Install Docker
2. Clone the repository.
3. Run `docker-compose up --build`
4. Use `docker-compose down` to clear images

## Install - Standalone
1. Install Node.js and Redis
2. Clone the repository.
3. Run Redis instance
4. Run `npm install`
5. Configure ENV variables (launch.json in VS code)
6. Run `npm run dev`
