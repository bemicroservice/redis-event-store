import RedisIO from "ioredis";
import Fastify from "fastify";
import StockEventGenerator from "./utils/stock-event-generator";
import { WsEmmiter, buildWebsocketServer } from "./utils/websocket-server";


const PORT = parseInt(process.env.PORT as string)
const HOST = process.env.HOST
const REDIS_HOST = process.env.REDIS_HOST
const REDIS_PORT = parseInt(process.env.REDIS_PORT as string)
const REDIS_PUBLISH_CHANNEL = process.env.REDIS_PUBLISH_CHANNEL
const REDIS_STREAM_NAME = process.env.REDIS_STREAM_NAME
const SEND_INTERVAL_MS = parseInt(process.env.SEND_INTERVAL_MS as string)

//REST HTTP Server
const fastify = Fastify({ logger: true })

//websockets (SockJS Node)
const wsEmmiter = new WsEmmiter()
const sockJsServer = buildWebsocketServer(wsEmmiter)

//redis client instance
const redis = new RedisIO(REDIS_PORT, REDIS_HOST)

//websocket server attatch
sockJsServer.installHandlers(fastify.server)

//events publish job (pubsub and websocket) + 
//save evenry event to event-store with timestamp
const publish_job = async (message: string) => {

  //publish events
  await redis.publish(REDIS_PUBLISH_CHANNEL, message)
  wsEmmiter.emit("stockevent", message)

  //save event to the named stream
  const timestamp = await redis.xadd(REDIS_STREAM_NAME, "*", "event", message)
  console.log(`Last saved timestamp ${timestamp}`)
}

fastify.get('/', async (request, reply) => {
  return { server: 'alive' }
})

/*** 
 * Return number of saved events in the stream
 */
fastify.get('/number-of-events', async (request, reply) => {
  const value = await redis.xlen(REDIS_STREAM_NAME)
  return { stream: REDIS_STREAM_NAME, numberOfEvents: value }
})

/***
 * Return the last saved event in the stream
 */
fastify.get('/last-event', async (request, reply) => {
  const value = await redis.xrevrange(REDIS_STREAM_NAME, "+", "-", "COUNT", "1")
  return { stream: REDIS_STREAM_NAME, lastEvent: value }
})

/**
 * Return (stream) all events saved in time range. Time parameters must be in ms timestamps
 */
fastify.get('/events-in-range/:from/:to', async (request, reply) => {
  const { from, to } = request.params as any

  if (from > to) reply.status(400).send({ error: "Wrong parameters" })

  const value = await redis.xrange(REDIS_STREAM_NAME, from, to)
  return { from, to, stream: REDIS_STREAM_NAME, values: value }
})

//app start and event mocking
const start = async () => {
  try {
    const sender = await StockEventGenerator.init()
    setInterval(async () => {
      const nextStockRecord = sender && sender.nextRecord
      await publish_job(JSON.stringify(nextStockRecord))
    }, SEND_INTERVAL_MS)
    await fastify.listen(PORT, HOST)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()