import Axios from "axios";
import { StockRecord } from "../models/StockRecord";

const DEMO_URL = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=IBM&interval=5min&apikey=demo"

export default class StockEventGenerator {

  private lastIndex = 0

  constructor(public allUpdates: StockRecord[]) {
  }

  get nextRecord(): StockRecord {
    const nextRecordValue = this.allUpdates[this.lastIndex]
    this.lastIndex === this.allUpdates.length - 1 ? this.lastIndex = 0 : this.lastIndex++
    return nextRecordValue
  }

  static async init() {
    try {
      const data = await Axios.get(DEMO_URL)
      const rawUpdates = data["data"]["Time Series (5min)"]
      const cleanUpdates = Object.keys(rawUpdates).map<StockRecord>(dateKey => ({
        timestamp: new Date(dateKey),
        open: rawUpdates[dateKey]["1. open"],
        high: rawUpdates[dateKey]["2. high"],
        low: rawUpdates[dateKey]["3. low"],
        close: rawUpdates[dateKey]["4. close"],
        volume: rawUpdates[dateKey]["5. volume"]
      })).sort((a, b) => a.timestamp.getTime() - b.timestamp.getTime())

      return new StockEventGenerator(cleanUpdates)
      
    } catch (error) {
      console.error(error)
    }
  }
}