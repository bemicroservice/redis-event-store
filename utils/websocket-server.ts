import SockJS from "sockjs";
import { EventEmitter } from "events";

export class WsEmmiter extends EventEmitter {
}

export const buildWebsocketServer = (eventEmmiter: WsEmmiter) => {
  const wsServer = SockJS.createServer()

  wsServer.on("connection", (wsConnection) => {
    
    eventEmmiter.on("stockevent", (message) => wsConnection.write(message))

    wsConnection.on("data", (data) => console.dir(data))
    wsConnection.on("close", () => console.log("closing connection"))
  })

  return wsServer
}